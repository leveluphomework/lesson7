# lesson7

Задание:
 - Развернуть тестовое окружение, используя docker-compose.

Выполнено:
    Подняты контейнеры:
        - elastisearch
        - logstash
        - kibana

Скрин работы: https://gitlab.com/leveluphomework/lesson7/-/blob/master/elkUp.png
